# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{
  pkgs ? import <nixpkgs> { },
}:
pkgs.callPackage ./package.nix { }
