<!--
SPDX-FileCopyrightText: 2021 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<Project
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:gnome="http://api.gnome.org/doap-extensions#"
  xmlns="http://usefulinc.com/ns/doap#"
>
  <name>Text Pieces</name>
  <shortdesc xml:lang="en">Developer's scratchpad</shortdesc>
  <description xml:lang="en">
    Powerful scratchpad with ability to perform a lot of text transformations.
  </description>

  <homepage rdf:resource="https://gitlab.com/liferooter/textpieces/" />
  <license rdf:resource="http://usefulinc.com/doap/licenses/gpl" />
  <bug-database rdf:resource="https://gitlab.com/liferooter/textpieces/-/issues" />
  <programming-language>Rust</programming-language>

  <repository>
    <GitRepository>
      <location rdf:resource="https://gitlab.com/liferooter/textpieces.git" />
      <browse rdf:resource="https://gitlab.com/liferooter/textpieces" />
    </GitRepository>
  </repository>

  <maintainer>
    <foaf:Person>
      <foaf:name>Gleb Smirnov</foaf:name>
      <foaf:mbox rdf:resource="mailto:glebsmirnov0708@gmail.com" />
      <foaf:account>
        <foaf:OnlineAccount>
            <foaf:accountServiceHomepage rdf:resource="https://github.com/" />
            <foaf:accountName>liferooter</foaf:accountName>
        </foaf:OnlineAccount>
      </foaf:account>
      <foaf:account>
        <foaf:OnlineAccount>
            <foaf:accountServiceHomepage rdf:resource="https://gitlab.com/" />
            <foaf:accountName>liferooter</foaf:accountName>
        </foaf:OnlineAccount>
      </foaf:account>
      <foaf:account>
        <foaf:OnlineAccount>
          <foaf:accountServiceHomepage
            rdf:resource="https://gitlab.gnome.org"
          />
          <foaf:accountName>liferooter</foaf:accountName>
        </foaf:OnlineAccount>
      </foaf:account>
    </foaf:Person>
  </maintainer>
</Project>
