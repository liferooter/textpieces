# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{ pkgs, fenv-src }:
pkgs.mkShell {
  packages =
    let
      fenv = pkgs.rustPlatform.buildRustPackage {
        pname = "fenv";
        version = "git";

        src = fenv-src;

        cargoLock.lockFile = "${fenv-src}/Cargo.lock";
      };
      flatpakWrapper =
        cmd:
        pkgs.writeShellApplication {
          name = cmd;

          runtimeInputs = [ fenv ];

          text = ''
            exec fenv exec -- ${cmd} "$@"
          '';
        };
    in
    (with pkgs; [
      flatpak-builder
      fenv
      pre-commit
    ])
    ++ (map flatpakWrapper [
      "cargo"
      "rust-analyzer"
      "meson"
      "ninja"
      "blueprint-compiler"
      "msgmerge"
      "msginit"
    ]);
}
