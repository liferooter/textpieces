// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::prelude::*;
use gtk::{gio, glib};

pub type GActions<'a, T> = &'a [(&'static str, for<'b> fn(&'b T))];

/// Converts easy-to-write declaration of `GAction`s into action entries.
pub fn to_entries<T: Clone + 'static, A: IsA<gio::ActionMap>>(
    actions: GActions<T>,
    target: &T,
) -> Vec<gio::ActionEntry<A>> {
    actions
        .iter()
        .copied()
        .map(|(name, callback)| {
            let target = target.clone();
            gio::ActionEntry::builder(name)
                .activate(move |_, _, _| callback(&target))
                .build()
        })
        .collect()
}
