// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gettextrs::pgettext;
use glib::prelude::*;
use glib::subclass::prelude::*;

use gtk::glib;

mod imp {
    use super::*;

    use std::cell::OnceCell;

    /// Private part of [`ActionModel`].
    ///
    /// [`ActionModel`]: super::ActionModel
    #[derive(Default, Debug, glib::Properties)]
    #[properties(wrapper_type = super::ActionModel)]
    pub struct ActionModel {
        /// Action ID.
        #[property(get, construct_only)]
        id: OnceCell<String>,

        /// Action name.
        #[property(get, construct_only)]
        name: OnceCell<String>,

        /// Action description.
        #[property(get, construct_only)]
        description: OnceCell<String>,

        /// Is the action built-in.
        #[property(get, construct_only)]
        is_builtin: OnceCell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ActionModel {
        const NAME: &'static str = "TextPiecesAction";
        type Type = super::ActionModel;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for ActionModel {}
}

glib::wrapper! {
    /// GObject wrapper around [`ActionModel`].
    pub struct ActionModel(ObjectSubclass<imp::ActionModel>);
}

impl ActionModel {
    /// Returns a GObject wrapper around [`ActionModel`].
    pub fn new(action_id: &str, name: &str, description: &str, is_builtin: bool) -> Self {
        glib::Object::builder()
            .property("id", action_id)
            .property("name", name)
            .property("description", description)
            .property("is-builtin", is_builtin)
            .build()
    }

    /// Returns translated action name,
    /// or non-translated if the action is custom.
    pub fn translated_name(&self) -> String {
        pgettext("action", self.name())
    }

    /// Returns translated description,
    /// or non-translated if the action is custom.
    pub fn translated_description(&self) -> String {
        pgettext("action", self.description())
    }
}
